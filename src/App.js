import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Pages/Home'

function App() {
  return (
    <Router basename={'/advotics'}>
      <Route path='/' component={Home} />
    </Router>
  );
}

export default App;
