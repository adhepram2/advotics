import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import '../../Assets/Style/SideBar.css';

export default class Sidebar extends Component {
    constructor(props){
        super(props);
        this.state = {
            redirect: undefined
        }
    }
    render() {
        return (
            <nav className="main-menu">
                <ul>
                    <li>
                        <Link to="/home">
                            <i className="fa fa-align-justify fa-2x fa-SideBar"></i>
                            <span className="nav-text">
                                Dashboard
                            </span>
                        </Link>
                    </li>
                    <li>
                        <Link to="/Home">
                            {/* <i className="fa fa-user-circle fa-2x fa-SideBar"></i> */}
                            <img className="image" src={require('../../Assets/Image/Dashboard.png').default} alt=""/>
                            <span className="nav-text">
                                List Pengguna
                            </span>
                        </Link>
                    
                    </li>
                </ul>
            </nav>
        )
    }
}
