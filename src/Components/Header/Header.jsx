import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import '../../Assets/Style/Header.scss'
import logo from '../../Assets/Image/Logo.jpg';

const $ = require('jquery');
export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hamburgerActive: false,
            searchMobileActive: false,
            profileMobileActive: false,
            /**
             * Web Version
             */
            searchIsOpen: true,
            /**
             * Modal State
             */
            modalVisible: false,
            /**
             * Action Edit
             */
            actionEdit: false,
        }
    }
   
    handleClickHamburgerActive = async() => {
        try {
            if(this.state.hamburgerActive === false) {
                this.setState({ hamburgerActive: true });
            }else if(this.state.hamburgerActive === true) {
                this.setState({ hamburgerActive: false, searchMobileActive: false, profileMobileActive: false });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleClickMobileActive = async() => {
        try {
            if(this.state.searchMobileActive === false) {
                this.setState({ searchMobileActive: true, profileMobileActive: false });
            }else if(this.state.searchMobileActive === true) {
                this.setState({ searchMobileActive: false });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleProfileMobileActive = async() => {
        try {
            if(this.state.profileMobileActive === false) {
                this.setState({ profileMobileActive: true, searchMobileActive: false });
            }else if(this.state.profileMobileActive === true) {
                this.setState({ profileMobileActive: false });
            }
        } catch (err) {
            console.log(err)
        }
    }
    /**
     * Web Version
     */
    handlesearchIsOpen = async() => {
        try {
            if(this.state.searchIsOpen === false) {
                this.setState({ searchIsOpen: true });
            }else if(this.state.searchIsOpen === true) {
                this.setState({ searchIsOpen: false });
            }
        } catch (err) {
            console.log(err);
        }
    }
    render() {
        /**
         * Animation Header
         */
        $(window).on('scroll', function() {
            // console.log($(this).scrollTop())
            if($(this).scrollTop() > 82) {
                if(!$('.c_advoticsheader').hasClass('expand')) {
                    $('.c_advoticsheader').addClass('expand');
                }
            }else {
                if($('.c_advoticsheader').hasClass('expand')) {
                    $('.c_advoticsheader').removeClass('expand');
                }
            }
        });

        return (
            <div className="c_advoticsheader">
                <div className="c_mainListMenuHeader">
                    <div className="c_leftHeader">
                        <Link className="c_advoticsheader_logo" to="/Home">
                            <img className="child_logoHeader" src={logo} alt="img-responsive"/>
                        </Link>
                    </div>

                    <div className="c_RightMenuHeader">
                        <div className="i_account">
                            Username <br/> Company Name
                        </div>
                        <div className="i_Right prnt_img">
                            <Link>
                                <img className='i_NavbarProfile' src={require('../../Assets/Image/Profile.svg').default} alt=""/>
                            </Link>
                        </div>
                        <div className="ii_right_logout prnt_img">
                            <Link>
                                <img className='i_NavbarProfile' src={require('../../Assets/Image/logout.png').default} alt=""/>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className={this.state.hamburgerActive === true ? 'menu_wrapper isOpen' : 'menu_wrapper'}>
                    <div className="headerMenuWrapper">
                        <img className="logoMenuWrapper" src={logo} alt="img-responsive"/>
                    </div>
                </div>
            </div>
        )
    }
}
