import React, { Component } from 'react'
import Header from '../Components/Header/Header'
import Sidebar from '../Components/Sidebar/Sidebar'
import moment from 'moment';
import 'bootstrap-daterangepicker/daterangepicker.css';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import { Bar } from '@reactchartjs/react-chart.js';
import swalert from 'sweetalert';
import Collapse from "@kunukn/react-collapse";
const responsiveJS = window.matchMedia("(max-width: 768px)");
const options = {
    scales: {
    yAxes: [
        {
        stacked: true,
        ticks: {
            beginAtZero: true,
        },
        },
    ],
    xAxes: [
        {
        stacked: true,
        },
    ],
    },
}
export default class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            collapse: true,
            // DATA DEFAULT RANGE DATE & HARI SEBELUM
            start: moment().subtract(6, 'days').toDate(),
            end: moment().toDate(),
            dataBestSelling : [
                {
                    img: require('../Assets/Image/Product.png').default,
                    name: 'Produk Satu',
                    price: '2.000.000',
                    qty: 21
                },{
                    img: require('../Assets/Image/Danone-Products.jpg').default,
                    name: 'Produk Dua',
                    price: '5.000.000',
                    qty: 23,
                },{
                    img: require('../Assets/Image/Product.png').default,
                    name: 'Produk Tiga',
                    price: '3.000.000',
                    qty: 12,
                },{
                    img: require('../Assets/Image/Product.png').default,
                    name: 'Produk Empat',
                    price: '6.000.000',
                    qty: 44,
                },{
                    img: require('../Assets/Image/Danone-Products.jpg').default,
                    name: 'Produk Lima',
                    price: '7.000.000',
                    qty: 2,
                }
            ],
            dataTopCom : [
                {
                    img: require('../Assets/Image/Danone-Products.jpg').default,
                    name: 'Produk Satu',
                    price: '2.000.000',
                    qty: 21
                },{
                    img: require('../Assets/Image/Danone-Products.jpg').default,
                    name: 'Produk Dua',
                    price: '5.000.000',
                    qty: 23,
                },{
                    img: require('../Assets/Image/Product.png').default,
                    name: 'Produk Tiga',
                    price: '3.000.000',
                    qty: 12,
                },{
                    img: require('../Assets/Image/Danone-Products.jpg').default,
                    name: 'Produk Empat',
                    price: '6.000.000',
                    qty: 44,
                },{
                    img: require('../Assets/Image/Product.png').default,
                    name: 'Produk Lima',
                    price: '7.000.000',
                    qty: 2,
                }
            ],
            dataChart: {
                labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                datasets: [
                {
                    type: 'line',
                    label: 'Total',
                    borderColor: '#FFFF00',
                    backgroundColor: '#FFFF00',
                    borderWidth: 2,
                    fill: false,
                    data: [18, 34, 39, 26, 28, 39],
                },
                {
                    label: 'Nett',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: '#3eab4d',
                },
                {
                    label: 'Gross',
                    data: [2, 3, 20, 5, 1, 4],
                    backgroundColor: '#2b9846',
                },
                {
                    label: 'Average Purchase',
                    data: [3, 10, 13, 15, 22, 30],
                    backgroundColor: '#83de94',
                },
                {
                    label: 'Unit Per Transaction',
                    data: [1, 2, 3, 1, 3, 2],
                    backgroundColor: '#707070',
                },
                ],
            }
        }
    }
    componentDidMount() {
    }
    handleCallback = (start, end, label) => {
        console.log(moment(start).format('DD MMMM YYYY'), moment(end).format('DD MMMM YYYY'));
        var months;
        months = (new Date(end).getFullYear() - new Date(start).getFullYear()) * 12;
        months -= new Date(start).getMonth();
        months += new Date(end).getMonth();
        console.log(months)
        if(months > 5) {
            swalert({
                title: 'Informasi',
                text: 'Peroid yang anda pilih lebih dari 6 bulan !',
                icon: 'warning',
            })
        } else {
            this.setState({
                start: moment(start).format('DD MMMM YYYY'),
                end: moment(end).format('DD MMMM YYYY')
            })
        }
    }
    render() {
        return (
            <div id='mainMenu'>
                <Header />
                {
                    responsiveJS.matches === true ? <div></div> : <Sidebar />
                }
                <div className="contentHome">
                    <div className="title">
                        <div className="Left">
                            <h2>Dashboard</h2>
                        </div>
                        <div className="right">
                            <DateRangePicker
                                initialSettings={{ startDate: this.state.start, endDate:  this.state.end, 
                                ranges: {
                                    Today: [moment().toDate(), moment().toDate()],
                                    Yesterday: [
                                    moment().subtract(1, 'days').toDate(),
                                    moment().subtract(1, 'days').toDate(),
                                    ],
                                    'Last 7 Days': [
                                    moment().subtract(6, 'days').toDate(),
                                    moment().toDate(),
                                    ],
                                    'Last 30 Days': [
                                    moment().subtract(29, 'days').toDate(),
                                    moment().toDate(),
                                    ],
                                    'This Month': [
                                    moment().startOf('month').toDate(),
                                    moment().endOf('month').toDate(),
                                    ],
                                    'Last Month': [
                                    moment().subtract(1, 'month').startOf('month').toDate(),
                                    moment().subtract(1, 'month').endOf('month').toDate(),
                                    ],
                                }, }}
                                onCallback={this.handleCallback}
                            >
                                <div
                                    id="reportrange"
                                    className="col-4"
                                    style={{
                                        background: '#FFFFFF',
                                        color: '#6A6A6A',
                                        cursor: 'pointer',
                                        padding: '5px 10px',
                                        border: '1px solid #ccc',
                                        justifyContent: 'space-between'
                                    }}
                                    >
                                    <i className="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;
                                    <span><small style={{fontSize: 16}}>Peroid</small>&nbsp;&nbsp;&nbsp;{moment(this.state.start).format('DD MMMM YYYY')} - {moment(this.state.end).format('DD MMMM YYYY')}</span> &nbsp;
                                    <i className="fa fa-caret-down"></i>
                                </div>
                            </DateRangePicker>
                        </div>
                    </div>
                    <div className="collapse" onClick={() => this.setState({collapse: !this.state.collapse})}>
                        <div className="left"><small>MARKET INSIGHTS</small></div>
                        <div className="right">
                            <img src={ this.state.collapse === true ? require('../Assets/Image/up.png').default : require('../Assets/Image/down.png').default} alt=""/>
                            &nbsp;&nbsp;<small onClick={() => console.log('allert')} style={{color: 'white', textDecoration: 'underline'}}>Click Here For Help</small>
                            &nbsp;&nbsp;<i className="fa fa-lightbulb-o" style={{ color:'white'}}> </i>
                        </div>
                    </div>
                    {/* COLLAPSE SCREEN */}
                        <Collapse isOpen={this.state.collapse}>
                            <div className="contentCard">
                                <div className="Card">
                                    <div className="top">
                                        <div className="left"><small>Sales Turnover</small></div>
                                        <div className="right"><img src={require('../Assets/Image/more.svg').default} alt="" /> </div>
                                    </div>
                                    <div className="bottom">
                                        <div className="left">
                                            <h2>Rp. 3,600,000</h2>
                                            <div className="info">
                                                {/* image dan color tergantung kondisi nanti saat terima api turun atau naik, dipantek karna menyesuaikan XD */}
                                                <img src={require('../Assets/Image/DownArrow.svg').default} alt=""/>
                                                <small style={{color: 'red' }}>13.8%</small>
                                                <small> last peroid in products sold</small>
                                            </div>
                                        </div>
                                        <div className="right"><img src={require('../Assets/Image/Sales.svg').default} alt=""/> </div>
                                    </div>
                                </div>
                            </div>
                            <div className="contentMenu">
                                <div className="CardChart">
                                    <div className="top">
                                        <div className="left"><h3>AVERAGE PURCHASE VALUE</h3></div>
                                        <div className="right">
                                            <img src={require('../Assets/Image/more.svg').default} alt=""/> 
                                            <select name="month" id="month">
                                                <option value="1">Last 1 Month</option>
                                                <option value="6">Last 6 Month</option>
                                                <option value="12">Last 12 Month</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="bottom">
                                        <Bar data={this.state.dataChart} options={options} height={responsiveJS.matches === true ? '' : 200} />
                                    </div>
                                </div>
                                <div className="cardBestSelling">
                                    <div className="top">
                                        <div className="left"><small>BEST SELLING SKU</small></div>
                                        <div className="right"><img src={require('../Assets/Image/more.svg').default} alt=""/></div>
                                    </div>
                                    <div className="contentItem">
                                        {
                                            this.state.dataBestSelling.map((value, key) => (
                                                <div className="card" style={{ backgroundColor: key === 0 ? '#FFE7BD' : '', height: key === 0 ? 90 : '' }}>
                                                    <div className="left"><img src={value.img} alt=""/></div>
                                                    <div className="right">
                                                        <small style={{ fontSize: key === 0 ? 20 : ''}}>{value.name}</small>
                                                        <div className="item">
                                                            <div className="price"><small>Rp {value.price}</small></div>
                                                            <div className="qty">{value.qty}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                                <div className="cardTopCom">
                                    <div className="top">
                                        <div className="left"><small>TOP COMPETITOR SKU</small></div>
                                        <div className="right"><img src={require('../Assets/Image/more.svg').default} alt=""/></div>
                                    </div>
                                    <div className="contentItem">
                                        {
                                            this.state.dataTopCom.map((value, key) => (
                                                <div className="card" style={{ backgroundColor: key === 0 ? '#FFE7BD' : '', height: key === 0 ? 90 : '' }}>
                                                    <div className="left"><img src={value.img} alt=""/></div>
                                                    <div className="right">
                                                        <small style={{ fontSize: key === 0 ? 20 : ''}}>{value.name}</small>
                                                        <div className="item">
                                                            <div className="price"><small>Rp {value.price}</small></div>
                                                            <div className="qty">{value.qty}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </Collapse>
                </div>
            </div>
        )
    }
}
